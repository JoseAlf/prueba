
CREATE DATABASE /*!32312 IF NOT EXISTS*/`appvidecomerce` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `appvidecomerce`;

/*Table structure for table `videos` */

DROP TABLE IF EXISTS `videos`;

CREATE TABLE `videos` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `idProducto` INT(11) NOT NULL,
  `nombre` CHAR(255) DEFAULT NULL,
  `codColor` CHAR(50) DEFAULT NULL,
  `referencia` CHAR(250) NOT NULL,
  `activo` TINYINT(4) NOT NULL DEFAULT '1',
  `fechaRegistro` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idProducto` (`idProducto`),
  CONSTRAINT `videos_ibfk_1` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `videos` */

INSERT  INTO `videos`(`id`,`idProducto`,`nombre`,`codColor`,`referencia`,`activo`,`fechaRegistro`) VALUES (1,2,'Video Vestidos de baño_02.mp4','color1','REF-VC1 - Color Blanco',1,'2018-08-23 06:55:49'),(2,2,'Video Vestidos de baño_03.mp4','color2','REF-V3 - Color Azul',1,'2018-08-23 06:54:50'),(3,2,'Video Vestidos de baño_01.mp4','color3','REF-V12 - Color Rosado',1,'2018-08-23 06:55:11'),(6,3,'Video Pijamas_02.mp4',NULL,'REF_PJ1',1,'2018-08-22 21:28:21'),(7,1,'Video Pijamas_01.mp4',NULL,'REF-PJ2',1,'2018-08-24 06:58:34'),(9,1,'Ejemplo 1',NULL,'algo',1,'2018-12-15 10:50:23');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
